<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/WEB-INF/include/header.jsp" %>
<%@ include file="/WEB-INF/include/navbar.jsp" %>

<c:url value="/recipe/edit" var="editRecipeUrl"/>
<c:url value="/recipe/delete" var="deleteRecipeUrl"/>

<c:url value="/rent/recipe" var="rentUrl"/>

<div class="container">

    <h1>List of recipes</h1>

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                <tr>
                    <th class="text-center col-md-1">Id</th>
                    <th class="text-center">Title</th>
                    <th class="text-center">Author</th>
                    <th class="text-center col-md-1">Available</th>
                    <th class="text-center col-md-1">Edit</th>
                    <th class="text-center col-md-1">Delete</th>
                    <th class="text-center col-md-1">Rent</th>

                </tr>
                </thead>
                <tbody>
                <c:forEach var="recipe" items="${recipeList}">
                    <tr>
                        <td>${recipe.id}</td>
                        <td>${recipe.title}</td>
                        <td>${recipe.author}</td>
                        <td>${recipe.available}</td>
                        <td class="text-center"><a href="${editRecipeUrl}/${book.id}" class="btn btn-sm btn-primary">Edit</a></td>
                        <td class="text-center">
                            <a href="${deleteRecipeUrl}/${recipe.id}" class="btn btn-sm btn-danger delete-button">Delete</a>
                        </td>

                        <td class="text-center">
                        <c:choose>
                            <c:when test="${recipe.available > 0}">
                                <a href="${rentUrl}/${recipe.id}" class="btn btn-info btn-sm">Rent</a>
                            </c:when>
                            <c:otherwise>
                                brak
                            </c:otherwise>
                        </c:choose>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

</div>

<script>

    $(function() {
        $('.delete-button').on('click', function(event) {
            console.log(event);
            event.preventDefault();
            var url = event.target.href;
            $.post(url)
            .done(function() {
                location.reload();
            });
        });
    });




</script>

<%@ include file="/WEB-INF/include/footer.jsp" %>