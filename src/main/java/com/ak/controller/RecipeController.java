package com.ak.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.ak.entity.Recipe;
import com.ak.service.RecipeService;

@Controller
public class RecipeController {
	@Autowired
	private RecipeService recipeService;
	
	@RequestMapping(value= "/recipes", method = RequestMethod.GET) 
	public String getAllRecipes(Model model) //relacja pomi�dzy modelem a widokiem
	{
		List<Recipe> list = recipeService.findAll();
		model.addAttribute("recipesList", list); //lista ksiazek jest przekazywana do modelu, trzeba pamietac ta nazwe ze wzgledu na jsp
		return "recipes"; //zeby zostala wyswietlona strona "books.jsp"
	}
	
	//pobranie widoku do edycji/dodania ksiazki
	@RequestMapping(value = "/recipes/create", method = RequestMethod.GET)
	public String getCreateRecipeForm(Model model){
		model.addAttribute("recipe", new Recipe());
		return "recipe_create";
	}

	//dopisac adnotacje zwiazana z edycja ksiazki
	//ta metoda przygotowuje odpowiedni obiekt ksiazki do edycji na podstawie id ksiazki
	//i zwraca odpowiedni widok
	@RequestMapping(value = "/recipes/edit/{id}", method = RequestMethod.GET)
	public String getEditRecipeForm(Model model, @PathVariable Long id){
		Recipe recipe = recipeService.findOne(id);
		model.addAttribute("recipe", recipe);
		
		return "recipe_create";
	}	
	
	//do obslugi przycisku "zapisz" klikanego po dodaniu nowej ksiazki albo edycji
	@RequestMapping(value = "/recipes/save", method = RequestMethod.POST)
	public String postCreateRecipe(@ModelAttribute @Valid Recipe recipe, BindingResult bindingResult){
		if(bindingResult.hasErrors()){
			return "recipe_create";
		}
		
		recipeService.save(recipe);
		
		return "redirect:/recipes"; //bez redirect zostanie wyswietlony tylko html, nie zostanie pobrany aktualny stan ksiazek/danych
	//jesli damy redirect oprocz wyswietlenia strony zostanie tez wywolana metoda kontrolera - getAllBooks i zobaczymy wsz. ksiazki
	}
	
	//metoda do usuwania ksiazki na podstawie id przekazanego jako parametr
	@RequestMapping(value = "/books/delete/{id}", method = RequestMethod.GET)
	public String deleteRecipe(@PathVariable Long id){ //nie dodajemy model bo nie potrzebujemy obiektu ksiazki
		recipeService.delete(id);
		return "redirect:/recipes";
	}
}
