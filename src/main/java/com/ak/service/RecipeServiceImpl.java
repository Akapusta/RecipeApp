package com.ak.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ak.dao.RecipeDao;
import com.ak.entity.Recipe;

@Service
public class RecipeServiceImpl implements RecipeService {

	@Autowired
	private RecipeDao recipeDao;
	
	@Override
	public List<Recipe> findAll() {
		return recipeDao.findAll();
	}

	@Override
	public Recipe findOne(Long id) {
		return recipeDao.findOne(id);
	}

	@Override
	public void save(Recipe rec) {
		recipeDao.save(rec);
		
	}

	@Override
	public void delete(Long id) {
		recipeDao.delete(id);
	}

}
