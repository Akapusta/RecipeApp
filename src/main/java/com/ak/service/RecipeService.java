package com.ak.service;

import java.util.List;

import com.ak.entity.Recipe;

public interface RecipeService {
	List<Recipe> findAll();
	Recipe findOne(Long id);
	void save(Recipe rec);
	void delete(Long id);
}
