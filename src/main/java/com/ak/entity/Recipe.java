package com.ak.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "recipes")
public class Recipe extends BaseEntity {
	
	@NotNull //komunikat w razie niepodania wartosci bedzie defaultowy - wygenerowany automatycznie przez spring
	@Column(name = "title")
	@Size(min=3, max=255, message = "Niepoprawna d�ugosc")
	private String title;
	
	@NotNull 
	@Column(name = "author")
	@Size(min=3, max=255, message = "Niepoprawna d�ugosc")
	private String author;
	
	@NotNull
	@Column(name = "description")
	private String description;
	
	@Column(name = "cost")
	private Integer cost;
	
	
	//aktualny stan mozliwych do wypozyczenia ksiazek
	@Min(0)
	private Integer available;
	
	public Recipe() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getAvailable() {
		return available;
	}

	public void setAvailable(Integer available) {
		this.available = available;
	}	
}
