package com.ak.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ak.entity.Recipe;

public interface RecipeDao extends JpaRepository<Recipe, Long> {

}
