package com.ak.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ak.entity.User;

public interface UserDao extends JpaRepository<User, Long> {
	User findByEmail(String email);
}
