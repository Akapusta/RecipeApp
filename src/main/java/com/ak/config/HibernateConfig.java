package com.ak.config;


import org.springframework.core.env.Environment;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration 
@PropertySource(value = {"classpath:hibernate.properties"})
@EnableJpaRepositories(basePackages="com.ak.dao")
public class HibernateConfig {
	
	@Autowired
	private Environment environment;

// framework zaautowireduje gdzies obiekt na podstawie tej definicji 
	@Bean
	public DataSource dataSource(){  //DataSource to interfejs
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		
		driverManagerDataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driver.class.name"));
		driverManagerDataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
		driverManagerDataSource.setUsername(environment.getRequiredProperty("jdbc.user.name"));
		driverManagerDataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
		
		return driverManagerDataSource;
	}
	
	//obiekt fabryki beanow encyjnych - dzieku temu bedzie mozliwe ORM mapowanie obiektowo relacyjne
	
	@Bean
	public EntityManagerFactory entityManagerFactory(){
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		
		//"pomocniczy obiekt zwiazany z ustawieniami hibernatea
		Properties properties = new Properties(); //w tym obiekcie mamy ustawienia
		properties.put("hibernate.hbm2ddl.auto", environment.getProperty("hibernate.hbm2ddl.auto"));
		properties.put("hibernate.show_sql", environment.getProperty("hibernate.show_sql"));
		properties.put("hibernate.format_sql", environment.getProperty("hibernate.format_sql"));
		properties.put("hibernate.generate_statistics", environment.getProperty("hibernate.generate_statistics"));
	
		//glowny obiekt fabryki beanow
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
		factoryBean.setPackagesToScan("com.ak.entity"); //obiekt fabryki dowiaduje na podstawie kt obiektow ma dzialac
		factoryBean.setJpaVendorAdapter(hibernateJpaVendorAdapter);
		factoryBean.setJpaProperties(properties);
		factoryBean.setDataSource(dataSource());
		
		//produkcja beanow encyjnych byla mozliwa dopiero gdy zostana zakonczone wszystkie ustawienia
		factoryBean.afterPropertiesSet(); 
		
		return factoryBean.getObject(); //dopiero wywolanie metody getObject tworzy obiekt, klasyczny builder design pattern
	}
	
	//definicja "robienia" obiektu do obsługi transakcji
	@Bean
	public PlatformTransactionManager transactionManager(){
		JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
		jpaTransactionManager.setEntityManagerFactory(entityManagerFactory());
		return jpaTransactionManager;
	}
	
}
