package com.ak.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ak.service.UserService;

@Configuration 
@EnableWebSecurity

public class SecurityConfig extends WebSecurityConfigurerAdapter{

	public static final int ENCODE_STRENGTH = 10; //im wyższa wartość tym trudniej rozszyfrować
	
	@Autowired
	private UserService userService;
	
	// "wlaczenie"/okreslenie szyfrowania
	
	@Override //konfiguracja zwiazana z szyfrowaniem hasla
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		//inicjalizujemy obiekt zwiazany z algorytmem szyfrowania w odpowiednim servisie zwiazanym z serwerem
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(ENCODE_STRENGTH);
		auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder);
	}
	
	//okreslic autoryzacje odnosnie metod kontrolera
	@Override //tu sa nadawane uprawnienia
	protected void configure(HttpSecurity httpSecurity) throws Exception{
		httpSecurity.authorizeRequests().antMatchers("/login").permitAll()  //metoda mowi ze jest wymagana autoryzacja, zalogowac moze sie kazdy
		.antMatchers("/register").permitAll()
		.antMatchers("/users/**", "/create-user").hasRole("ADMIN") //zeby wykonywac operacje na uzytkownikach trzeba byc adminem
		.antMatchers("/admin/**").hasRole("ADMIN")
		.antMatchers("/**").authenticated() //dla bezpieczenstwa ma byc autoryzacja
		.antMatchers("/api/**").permitAll() 
		.antMatchers("/resources/**").permitAll() //dostep do danych
		.and() // i dodatkowo jesli chodzi o formularz logowania
		.formLogin()  //skad maja byc brane dane do logowania
		.usernameParameter("email") //email ma byc pobierany z kontrolki z taka nazwa
		.passwordParameter("password")
		.loginPage("/login") //forma logowania jest zwiazana z tym
		.and()
		.logout() 
		.logoutUrl("/logout") //jaka strona jest zwiazana z wylogowywaniem
		.logoutSuccessUrl("/logout?logout") //dodatkowo bedzie informacja czy sie udalo wylogowac
		.and()
		.csrf().disable(); //opcjonalnie: mozna wylaczyc metode zwiazana z zapobieganiem atakom cross site request forgery
	}

}
