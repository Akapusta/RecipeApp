package com.ak.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration 
@EnableWebMvc
@ComponentScan(basePackages = "com.ak")
@EnableTransactionManagement

public class AppConfig extends WebMvcConfigurerAdapter {

	private final static String email = "test5432101@gmail.com";
	private final static String password = "testpass123";
	
	@Override //metoda ktora wlacza domyslna konfiguracje
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer conf){
		conf.enable(); 
	}
	
	@Bean //zwraca obiekt ktory zawiera informacje o widoku
	public ViewResolver viewResolver(){
			InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
					viewResolver.setPrefix("/WEB-INF/");
					viewResolver.setSuffix(".jsp");
					
					return viewResolver;
		}
	
	//fabryka obiektu - przygotowuje w dopowiedni sposob obiekt ktory bedzie autowiredowany
@Bean
public JavaMailSender javaMailSender(){
	JavaMailSenderImpl javaMailSenderImpl = new JavaMailSenderImpl();
	javaMailSenderImpl.setHost("smtp.gmail.com");
	javaMailSenderImpl.setPort(587);
	
	javaMailSenderImpl.setUsername(email);
	javaMailSenderImpl.setPassword(password);
	
	javaMailSenderImpl.getJavaMailProperties().setProperty("mail.smtp.auth", "true");
	javaMailSenderImpl.getJavaMailProperties().setProperty("mail.smtp.startlls", "true");
	
	return javaMailSenderImpl;
}


}
